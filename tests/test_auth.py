import datetime
# import pytz
# import allure
from selenium import webdriver
import time
from selenium.webdriver.common.by import By

from locators import *


# @allure.description("Авторизация")
def test_authorization():
    driver = webdriver.Chrome()

    driver.implicitly_wait(5)

    driver.get(base_url)  # Ссылка с закрытым алертом
    driver.maximize_window()
    # with allure.step("Нажать кнопку Войти"):
    login_button = driver.find_element(By.XPATH, "//span[text()='Войти']")
    login_button.click()

    # with allure.step("Ввести емейл"):
    email = driver.find_element(By.CSS_SELECTOR, "[name='email']")
    email.send_keys(login_admin)

    # with allure.step("Ввести пароль"):
    password_form = driver.find_element(By.CSS_SELECTOR, "[class='mb-4 relative block']")
    password_form.send_keys(pass_admin)

    # with allure.step("Нажать кнопку Войти"):
    sign_in_button = driver.find_element(By.CSS_SELECTOR, "[type = 'submit']")
    sign_in_button.click()

    time.sleep(5)

    now_date = datetime.datetime.utcnow().strftime("%d.%m.%Y %H.%M")
    name_screenshot = "authorization " + now_date + ".png"
    driver.save_screenshot('./screen/' + name_screenshot)

    text_cabinet = driver.find_element(By.CSS_SELECTOR, "[class='title-h3 font-medium text-content md:text-2xl']")
    value_text_cabinet = text_cabinet.text
    driver.quit()

    assert value_text_cabinet == "Личный кабинет"
    print("Тест пройден успешно")


