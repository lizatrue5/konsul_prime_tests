from selenium import webdriver
# import time
from selenium.webdriver.common.by import By

from locators import *


def test_registration():
    driver = webdriver.Chrome()
    driver.implicitly_wait(5)

    driver.get(base_url)  # Ссылка с закрытым алертом
    login_button = driver.find_element(By.XPATH, "//span[text()='Войти']")
    login_button.click()

    sign_up_button = driver.find_element(By.XPATH, "//span[text()='Зарегистрироваться']")
    sign_up_button.click()

    last_name = driver.find_element(By.CSS_SELECTOR, "[name='last_name']")
    last_name.click()
    last_name.send_keys("Фамилия")

    name = driver.find_element(By.CSS_SELECTOR, "[name='first_name']")
    name.click()
    name.send_keys("Имя")

    second_name = driver.find_element(By.CSS_SELECTOR, "[name='patronymic']")
    second_name.send_keys("Отчество")

    data_birth = driver.find_element(By.NAME, "date")
    data_birth.send_keys("01.01.1999")

    email_sign = driver.find_element(By.CSS_SELECTOR, "[name='email']")
    email_sign.send_keys(mail_reg)

    pass_sign = driver.find_element(By.CSS_SELECTOR, "[type='password']")
    pass_sign.send_keys(base_pass)

    conf_pass = driver.find_element(By.CSS_SELECTOR, "[class='mb-7 relative block']")
    conf_pass.send_keys(base_pass)

    agreement_with_terms = driver.find_element(By.CSS_SELECTOR, "[class='typography']")
    agreement_with_terms.click()

    confirm_button = driver.find_element(By.CSS_SELECTOR, "[type='submit']")
    confirm_button.click()
    # time.sleep(3)

    driver.get(mail_url)
    # time.sleep(3)
    new_mail = driver.find_element(By.CSS_SELECTOR, "[class='msglist-message row ng-scope']")
    new_mail.click()
    # time.sleep(3)

    link_to_conf_pass = driver.find_element(By.CSS_SELECTOR, "#preview-plain a")
    link_to_conf_pass = link_to_conf_pass.get_attribute('href')

    driver.get(link_to_conf_pass)

    # time.sleep(5)

    text_successful_reg = driver.find_element(By.CSS_SELECTOR, "[class='text-base text-content']")
    text_successful_reg = text_successful_reg.text

    driver.quit()

    assert text_successful_reg == "Регистрация успешно завершена"
    print("Тест пройден успешно")
